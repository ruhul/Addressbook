<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Phone Book</title>

        <link href="../../../Resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        
        <link href="../../../Resource/css/style.css" rel="stylesheet">
		
        
    </head>
	
	<body>
		<div class=" wrapper">
		
			<div class="header">
				<h4>Phone Book</h4>
			</div>
			
		<div class="main_content">
		<div align="center"><h4>Add New Contract</h4></div>
			
			<div class="editing_info">

				<!-- Nav tabs -->
				<ul class="nav nav-tabs nav_tabs" role="tablist">
				<li role="presentation" class="active"><a href="#create" aria-controls="create" role="tab" data-toggle="tab">Create New</a></li>
			
				</ul>

				<!-- Tab panes -->
				<div class="tab-content ">
					<div role="tabpanel" class="tab-pane fade in active" id="create">
					<form class="form-inline" action="store.php" method="post" >
						
						<div>
							<div style="float:left" class="form-group bottom_tab">
								<label for="exampleInputName2">First Name</label>
								<input type="text" class="form-control" id="exampleInputName2" placeholder="write here" name="fname" />
							</div style="float:left" >
							<div style="float:right" class="">
								<input style="width: 100px; height: 100px;"type="file" name="photo" class="form-control" id="field2">
								<br />
								<label for="field2">Upload Image</label>
							</div>
							<div class="form-group bottom_tab">
								<label for="exampleInputName2">Last Name</label>
								<input type="text" class="form-control" id="exampleInputName2" placeholder="write here" name="lname" />
							</div>
							
						</div>
						
						
						<div>
							<div class="form-group bottom_tab">
								<label for="exampleInputEmail2">Mobile Number</label>
								<input type="text" class="form-control" id="exampleInputEmail2" placeholder="01*********" name="mno" />
							</div>
							<div class="form-group bottom_tab">
								<label for="exampleInputEmail2">Home Number</label>
								<input type="text" class="form-control" id="exampleInputEmail2" placeholder="01*********" name="hno" />
							</div>
						  <div class="form-group bottom_tab">
							<label for="exampleInputEmail2">Office Number</label>
							<input type="text" class="form-control" id="exampleInputEmail2" placeholder="01*********"name="ofno" />
						  </div>
						  <div class="form-group bottom_tab">
							<label for="exampleInputEmail2">Others Number</label>
							<input type="text" class="form-control" id="exampleInputEmail2" placeholder="01*********" name="otno" />
						  </div>
						  <div class="form-group bottom_tab">
							<label for="exampleInputEmail2">Email Adderss</label>
							<input type="email" class="form-control" id="exampleInputEmail2" placeholder="*****@yourmail.com" name="email" />
						  </div>
						   <div class="form-group bottom_tab radio">
							  <label>Sex</label>&nbsp;&nbsp;
							  <label>
								<input type="radio" class="form-control" name="gender" value="Male" checked="checked">Male
							  </label>
								&nbsp;&nbsp;
							  <label>
								<input type="radio" class="form-control" name="gender" value="Female">Female
							  </label>
							  
							</div><br>
						  <div class="form-group bottom_tab">
							<label for="exampleInputEmail2">City</label>
							<input type="test" class="form-control" id="exampleInputEmail2" placeholder="City Name" name="city" />
						  </div>
						  <div class="form-group bottom_tab">
							<label for="exampleInputEmail2">Address</label>
							<input type="text" class="form-control" id="exampleInputEmail2" placeholder="Full Address" name="address" />
						  </div>
						  <div class="form-group bottom_tab">
							<label>Services</label>
							<select class="form-control"name="service">
								<option>Govt. Job</option>
								<option>Private Job</option>
								<option>Banker</option>
								<option>Scientist</option>
								<option>Engineer</option>
								<option>Student</option>
							</select>
							</div>
						  </div><br>
							
						<div class="button">
							  <input type="reset" class="btn btn-danger">
							  <button type="submit" class="btn btn-success" name="submit">SAVE</button>
							  <a class="btn btn-info" href="index.php">Back TO List</a>
						 
						</div>  
					</form>
					</div>
				</div>

			</div>
			</div>
		</div>
		
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>
	</body>
	
</html>	